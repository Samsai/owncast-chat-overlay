#!/usr/bin/env bash
set -euo pipefail

cargo build --release

mkdir -p build
cp target/release/chat-overlay build/
cp -r template build/

rsync -r -a -v -e ssh --delete-after build root@samsai.eu:/home/chatoverlay/chat-overlay
ssh root@samsai.eu chown chatoverlay /home/chatoverlay/chat-overlay
