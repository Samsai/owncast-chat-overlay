use axum::Json;
use axum::http::StatusCode;
use axum::response::Sse;
use axum::response::sse::KeepAlive;
use axum::routing::post;
use tokio_stream::wrappers::BroadcastStream;
use tokio_stream::wrappers::errors::BroadcastStreamRecvError;
use futures_util::stream::Stream;

use tokio::sync::broadcast;
use tokio::sync::broadcast::Sender;

use std::env;
use std::net::SocketAddr;
use std::sync::Arc;

use axum::{
    extract::State,
    response::Html,
    routing::get,
    Router,
};

use axum_client_ip::InsecureClientIp;

use tera::Tera;
use tera::Context;

use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
struct Event {
    r#type: String,
    eventData: EventData,
}

#[derive(Deserialize, Serialize, Debug)]
struct EventData {
    user: User,
    body: String,
    rawBody: String,
    id: String,
    visible: bool,
    timestamp: String,
}

#[derive(Deserialize, Serialize, Debug)]
struct User {
    id: String,
    displayName: String,
    displayColor: i32,
    createdAt: String,
    previousNames: Vec<String>,
}

#[derive(Clone)]
struct AppState {
    tera: Tera,
    sender: Arc<Sender<axum::response::sse::Event>>
}

#[tokio::main]
async fn main() {
    let (tx, _rx) = broadcast::channel(16);

    let sender = Arc::new(tx);

    let mut tera = Tera::new("template/*.html").expect("Failed to compile Tera templates");
    tera.autoescape_on(vec![]);

    let port = env::var("PORT").unwrap_or("3030".to_string());

    let app = Router::new()
        .route("/", get(chat_overlay))
        .route("/messages", get(sse_handler))
        .route("/owncast", post(owncast_event))
        .with_state(AppState { tera, sender });

    axum::Server::bind(&format!("0.0.0.0:{}", port).parse().unwrap())
        .serve(
            app.into_make_service_with_connect_info::<SocketAddr>()
        )
        .await
        .unwrap();
}

async fn chat_overlay(
    State(state): State<AppState>
) -> Html<String> {
    let mut context = Context::new();
    
    Html(state.tera.render("page.html", &mut context).expect("Failed to render page"))
}

async fn sse_handler(
    State(state): State<AppState>
) -> Sse<impl Stream<Item = Result<axum::response::sse::Event, BroadcastStreamRecvError>>> {
    let stream = BroadcastStream::new(state.sender.subscribe());

    Sse::new(stream).keep_alive(KeepAlive::default())
}

async fn owncast_event(
    State(state): State<AppState>,
    insecure_ip: InsecureClientIp,
    event: Json<Event>
) -> StatusCode {
    if insecure_ip.0.is_loopback() {
        // Owncast img URLs are relative, so we need to turn them into absolute
        let owncast_url = env::var("OWNCAST_URL").unwrap_or("https://cast.samsai.eu".to_string());
        let message = event.eventData.body
            .replace("src=\"/img/", &format!("src=\"{}/img/", owncast_url));

        let mut context = Context::new();

        context.insert("username", &event.eventData.user.displayName);
        context.insert("message_body", &message);

        let element = state.tera.render("message.html", &mut context).expect("Failed to render message");

        state.sender.send(
            axum::response::sse::Event::default()
                .event("message")
                .data(element)
        ).expect("Couldn't send event");
        StatusCode::OK
    } else {
        StatusCode::FORBIDDEN
    }
}
